import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, classification_report, confusion_matrix, roc_auc_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier

data = pd.read_csv('dataset.csv')

# 'features' - признаки
# 'target' - депрессия/нет депрессии
features = ['k1', 'k2', 'k3', 'k4', 'k5', 'k6', 'k7', 'k8', 'k9', 'Sanity', 'Health', 'DialogsCount', 'APM', 'LongTerm', 'ShortTerm']
target = 'ConfirmedDepression'

X = data[features]
y = data[target]

# Разделение данных на обучающую и тестовую выборки
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Функция для вывода метрик
def print_metrics(y_test, y_pred, model_name):
    accuracy = accuracy_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    f1 = f1_score(y_test, y_pred)
    f1_weighted = f1_score(y_test, y_pred, average='weighted')
    roc_auc = roc_auc_score(y_test, y_pred)
    
    print(f"{model_name} Accuracy: {accuracy:.4f}")
    print(f"{model_name} Precision: {precision:.4f}")
    print(f"{model_name} Recall: {recall:.4f}")
    print(f"{model_name} F1 Score: {f1:.4f}")
    print(f"{model_name} F1 Score (weighted): {f1_weighted:.4f}")
    print(f"{model_name} ROC AUC: {roc_auc:.4f}")
    print(f"{model_name} Classification Report: \n{classification_report(y_test, y_pred)}")
    print(f"{model_name} Confusion Matrix: \n{confusion_matrix(y_test, y_pred)}")
    print("\n")

# Обучение и оценка моделей

# Decision Tree
dt_model = DecisionTreeClassifier(random_state=42)
dt_model.fit(X_train, y_train)
dt_predictions = dt_model.predict(X_test)
print_metrics(y_test, dt_predictions, "Decision Tree")

# SVM (linear)
svm_model = SVC(kernel='linear', random_state=42)
svm_model.fit(X_train, y_train)
svm_predictions = svm_model.predict(X_test)
print_metrics(y_test, svm_predictions, "SVM (linear)")

# kNN
knn_model = KNeighborsClassifier(n_neighbors=5)
knn_model.fit(X_train, y_train)
knn_predictions = knn_model.predict(X_test)
print_metrics(y_test, knn_predictions, "kNN")

# Random Forest
rf_model = RandomForestClassifier(random_state=42)
rf_model.fit(X_train, y_train)
rf_predictions = rf_model.predict(X_test)
print_metrics(y_test, rf_predictions, "Random Forest")