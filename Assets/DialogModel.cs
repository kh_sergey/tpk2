using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class DialogModel : MonoBehaviour
{
    public void OnSelect()
    {
        GlobalVariables.Instance.joystick.SetActive(true);
        gameObject.SetActive(false);
    }
}
