using System;
using DefaultNamespace;
using UnityEngine;

public class DialogCollider : MonoBehaviour
{
    [SerializeField]
    private GameObject[] activated;
    
    [SerializeField]
    private GameObject[] deactivated;

    private void OnTriggerEnter(Collider other)
    {
        foreach (GameObject element in activated)
        {
            element.SetActive(true);
        }
        
        foreach (GameObject element in deactivated)
        {
            element.SetActive(false);
        }

        GlobalVariables.Instance.joystick = GameObject.Find("JoystickArea");
        GlobalVariables.Instance.joystick.SetActive(false);
    }
}