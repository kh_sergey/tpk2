﻿using CodeBase.Infrastructure.AssetManagement;
using CodeBase.Infrastructure.Factory;

namespace CodeBase.Infrastructure.Services
{
    public class AllServices
    {
        private static AllServices _instance;
        public static AllServices Container => _instance ?? (_instance = new AllServices());

        public void RegisterSingle<TService>(TService implementation) where TService : IService => 
            Implementation<TService>.ServiceInstance = implementation;

        public TService Single<TService>() where TService : IService => 
            Implementation<TService>.ServiceInstance;

        private static class Implementation<TService> where TService : IService // При компиляции на каждый случай использования появится свой статический класс
        {                                                                       // Например Implementation<IGameService>, Implementation<IAssets> и т.д.
            public static TService ServiceInstance;                             // Поэтому такой трюк со статикой и работает
        }                                                                       // Т.е. внутри AllServices будет много приватных Implementation на каждый используемый сервис
    }
}