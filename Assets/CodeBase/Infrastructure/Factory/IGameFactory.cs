﻿using System.Collections.Generic;
using CodeBase.Infrastructure.Services;
using CodeBase.Infrastructure.Services.PersistentProgress;
using CodeBase.StaticData;
using UnityEngine;

namespace CodeBase.Infrastructure.Factory
{
    public interface IGameFactory : IService
    {
        List<ISavedProgressReader> ProgressReaders { get; }
        List<ISavedProgress> ProgressWriters { get; }

        GameObject CreateHud();
        GameObject CreateHero(GameObject at);
        GameObject CreateMonster(MonsterTypeId typeId, Transform parent);
        GameObject CreateLoot();

        void Cleanup();
        void Register(ISavedProgressReader progressReader);
    }
}