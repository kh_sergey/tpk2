﻿using System.Collections.Generic;
using UnityEngine;

namespace CodeBase.Cards
{
    public class CardMatchChecker
    {
        private readonly int _gap;

        public CardMatchChecker(int gap)
        {
            _gap = gap;
        }
        
        /// <summary>
        /// Проверяем текущую карту на матч с gap-предыдущей картой
        /// </summary>
        /// <returns></returns>
        public bool CheckMatch(List<Card> cards)
        {
            Card currentCard = cards[^1];
            Card gappedCard = cards[^_gap];
            
            return gappedCard != null && currentCard.Equals(gappedCard);
        }
    }
}