﻿using System;
using System.Collections;
using System.Collections.Generic;
using CodeBase.Cards.Statistics;
using CodeBase.Infrastructure;
using UnityEngine;

namespace CodeBase.Cards
{
    /// <summary>
    /// Геймплейная логика
    /// </summary>
    public class CardGame
    {
        private readonly ICoroutineRunner _coroutineRunner;
        private readonly CardGameSettings _settings;
        private readonly CardFabric _cardFabric;
        private readonly CardMatchChecker _matchChecker;
        private readonly CardGameStatistics _statistics;

        private List<Card> _cards;
        private int _currentCardIndex; // индекс текущей итерируемой карты
        private bool _isClickable;
        private bool _matchMade;

        public event Action OnStarted;
        public event Action OnEnded;
        public Func<float> OnShowCard; // todo: для анимок, возвращают время на анимацию
        public Func<float> OnHideCard;

        // todo: делать инъекции
        public CardGame(ICoroutineRunner coroutineRunner, CardGameSettings settings, CardFabric cardFabric, CardMatchChecker matchChecker, CardGameStatistics statistics)
        {
            _coroutineRunner = coroutineRunner;
            _settings = settings;
            _cardFabric = cardFabric;
            _matchChecker = matchChecker;
            _statistics = statistics;
        }

        public void StartGame() => 
            _coroutineRunner.StartCoroutine(GameRoutine());

        // todo: вызывается по любому клику на экране
        public void CheckMatch()
        {
            if (!_isClickable || _matchMade)
                return;

            bool result = _matchChecker.CheckMatch(_cards);
            _matchMade = true;
            
            float sessionTime = 0; // todo
            float roundTime = 0;
            
            SetMatch(sessionTime, roundTime, result ? CardRoundData.Result.True : CardRoundData.Result.False);
        }

        private void SetTimedOutMatch() =>
            SetMatch(-1, -1, CardRoundData.Result.TimeOut);
        
        private void SetMatch(float sessionTime, float roundTime, CardRoundData.Result result) => 
            _statistics.SetMatch(_currentCardIndex, sessionTime, roundTime, result);

        private IEnumerator GameRoutine()
        {
            _cards.Clear();
            
            OnStarted?.Invoke();
            
            for (int i = 0; i < _settings.CardsToGenerate; i++)
            {
                _cards.Add(_cardFabric.GetCard());
                _matchMade = false;
                
                // todo: anim in
                float? showTime = OnShowCard?.Invoke() ?? 0;
                yield return new WaitForSeconds(showTime.Value);

                _isClickable = true;
                yield return _settings.RoundTime;
                
                if (!_matchMade)
                    SetTimedOutMatch();
                _isClickable = false;
                
                // todo: anim out
                float? hideTime = OnHideCard?.Invoke() ?? 0;
                yield return new WaitForSeconds(hideTime.Value);
            }
            
            // todo: ShowScore
            OnEnded?.Invoke();
        }
    }
}