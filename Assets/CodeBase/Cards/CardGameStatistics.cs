﻿using CodeBase.Cards.Statistics;

namespace CodeBase.Cards
{
    /// <summary>
    /// Модель данных статистики игры
    /// </summary>
    public class CardGameStatistics
    {
        private readonly CardGameSettings _settings;

        private CardRoundData[] _data;
        
        public CardGameStatistics(CardGameSettings settings)
        {
            _settings = settings;
            
            _data = new CardRoundData[_settings.CardsToGenerate];
        }
        
        public void SetMatch(int index, float sessionTime, float roundTime, CardRoundData.Result result)
        {
            _data[index] = new CardRoundData(sessionTime, roundTime, result);
        }
    }
}