﻿using System;
using CodeBase.Infrastructure;
using UnityEngine;

namespace CodeBase.Cards
{
    public class CardBootstrapper : MonoBehaviour, ICoroutineRunner
    {
        private CardGame _game;
        private CardFabric _cardFabric;
        private CardMatchChecker _matchChecker;
        private CardGameStatistics _statistics;

        public event Action<int, Action> OnShowReward; // todo: на эту штуку подписывается вьюха, по кнопке идёт выдача награды

        public void StartGame(CardGameSettings _settings)
        {
            _cardFabric = new CardFabric(_settings.NumbersRange, _settings.ColorsRange);
            _matchChecker = new CardMatchChecker(_settings.Gap);
            _statistics = new CardGameStatistics(_settings);
            _game = new CardGame(this, _settings, _cardFabric, _matchChecker, _statistics);
            _game.OnEnded += OnGameEnded;
            _game.StartGame();
        }

        private void OnGameEnded()
        {
            // на этом уровне высчитываем награду, прокидываем в отображение вместе с Action выдачи
            Action rewardAction = () => Debug.Log("Your's reward"); // todo: action на выдачу награды
            int goldCount = 1;

            OnShowReward?.Invoke(goldCount, rewardAction);
        }
    }
}