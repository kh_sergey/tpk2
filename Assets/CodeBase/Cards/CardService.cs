﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CodeBase.Cards
{
    public class CardService : MonoBehaviour
    {
        [SerializeField]
        private float generateCooldown = 3;

        private CardFabric _cardFabric;
        private CardMatchChecker _cardMatchChecker;
        private List<Card> _generatedCards;

        private int _cardsToGenerate = 5; // todo : выбираться
        private int _points;

        [Button]
        private void StartGame()
        {
            // todo: должны выбираться от сложности
            _cardFabric = new CardFabric(3, 3);
            _cardMatchChecker = new CardMatchChecker(1);

            StartCoroutine(GenerateCardsRoutine());
        }

        private IEnumerator GenerateCardsRoutine()
        {
            for (int i = 0; i < _cardsToGenerate; i++)
            {
                yield return new WaitForSeconds(generateCooldown);

                GenerateCard();
            }
        }

        [Button]
        private void OnTap()
        {
            if (_cardMatchChecker.CheckMatch(_generatedCards))
                _points++;
        }

        private void GenerateCard()
        {
            _generatedCards.Add(_cardFabric.GetCard());
        }
    }
}