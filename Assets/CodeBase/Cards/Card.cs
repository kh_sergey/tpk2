﻿using System;
using UnityEngine;

namespace CodeBase.Cards
{
    public class Card
    {
        public readonly int Number;
        public readonly Color Color;

        public Card(int number, Color color)
        {
            Number = number;
            Color = color;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Card);
        }

        private bool Equals(Card other)
        {
            return Number == other.Number 
                   && Color == other.Color;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Number, Color);
        }
    }
}