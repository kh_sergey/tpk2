using UnityEngine;

[CreateAssetMenu(menuName = "Static/CardGameSettings")]
public class CardGameSettings : ScriptableObject
{
    public int CardsToGenerate = 5;
    public int NumbersRange = 3;
    public int ColorsRange = 3;
    public int Gap = 1;
    public int GoldReward = 10;
    public float RoundTime = 5;
}
