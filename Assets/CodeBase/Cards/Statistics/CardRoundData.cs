﻿namespace CodeBase.Cards.Statistics
{
    public class CardRoundData
    {
        public enum Result
        {
            True,
            False,
            TimeOut, // Не был дан ответ
        }

        /// <summary>
        /// Время матча с начала сессии
        /// -1 если TimeOut
        /// </summary>
        public float SessionTime { get; }

        /// <summary>
        /// Время матча с начала раунда
        /// -1 если TimeOut
        /// </summary>
        public float RoundTime { get; }

        public Result MatchResult { get; }

        public CardRoundData(float sessionTime, float roundTime, Result result)
        {
            SessionTime = sessionTime;
            RoundTime = roundTime;
            MatchResult = result;
        }
    }
}