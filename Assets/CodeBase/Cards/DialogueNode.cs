﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CodeBase.Cards
{
    [CreateAssetMenu(menuName = "Static/DialogueNodeSettings")]
    public class DialogueNode : ScriptableObject
    {
        public string SpeakerName;
        public string[] Paragraphs;
        public Answer[] Answers;
    }

    [Serializable]
    public class Answer
    {
        public string Text;
        public Criterion Criterion = Criterion.None;
        [HideIf(nameof(Cards.Criterion), Criterion.None)]
        public float Multiplier;
        public float GoldReward;
    }

    public enum Criterion
    {
        None,
        k1,
        k2,
        k3,
        k4,
        k5,
        k6,
        k7,
        k8,
        k9,
    }
}