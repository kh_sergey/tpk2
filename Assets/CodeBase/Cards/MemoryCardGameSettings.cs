﻿using UnityEngine;

namespace CodeBase.Cards
{
    [CreateAssetMenu(menuName = "Static/MemoryCardGameSettings")]
    public class MemoryCardGameSettings : ScriptableObject
    {
        public Vector2 CardsGrid = new Vector2(5,5);
        public int HintsCount = 3;
        public int CardsRange = 3;
        public int ColorRange = 1;
        public int GoldReward = 10;
        public float RoundTime = 10;
    }
}