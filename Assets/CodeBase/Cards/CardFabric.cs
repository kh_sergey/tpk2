using UnityEngine;

namespace CodeBase.Cards
{
    public class CardFabric
    {
        private readonly Color[] _colors = 
        {
            Color.red,
            Color.green, 
            Color.blue, 
            Color.yellow,
        };

        private readonly int _numbersRange;
        private readonly int _colorsRange;

        public CardFabric(int numbersRange, int colorsRange)
        {
            _numbersRange = numbersRange;
            _colorsRange = colorsRange;
        }
        
        public Card GetCard()
        {
            // берём рандомную цифру и цвет из рейнджа
            int generatedNumber = Random.Range(1, _numbersRange);
            Color generatedColor = _colors[Random.Range(1, _colorsRange)];
            
            Card card = new(generatedNumber, generatedColor);
            return card;
        }
    }
}