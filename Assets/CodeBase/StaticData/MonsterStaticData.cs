﻿using UnityEngine;

namespace CodeBase.StaticData
{
    [CreateAssetMenu(fileName = "MonsterData", menuName = "StaticData/Monster")]
    public class MonsterStaticData : ScriptableObject
    {
        public MonsterTypeId MonsterTypeId;
        
        public int HP;
        public float Damage;

        public int MinLoot;
        public int MaxLoot;
        
        [Range(1f, 10f)]
        public float MoveSpeed;

        [Range(0.1f, 5f)]
        public float EffectiveDistance;

        [Range(0.5f, 5f)]
        public float Cleavage;

        public GameObject Prefab;
    }
}