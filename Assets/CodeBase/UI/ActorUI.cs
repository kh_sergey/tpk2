﻿using System;
using CodeBase.Hero;
using CodeBase.Logic;
using UnityEngine;

namespace CodeBase.UI
{
    public class ActorUI : MonoBehaviour
    {
        public HPBar HPBar;

        private IHealth _heroHealth;

        private void OnDestroy()
        {
            if (_heroHealth != null)
                _heroHealth.HealthChanged -= UpdateHPBar;
        }

        private void Start()
        {
            IHealth health = GetComponent<IHealth>();

            if (health != null)
                Construct(health);
        }

        public void Construct(IHealth health)
        {
            _heroHealth = health;

            _heroHealth.HealthChanged += UpdateHPBar;
        }

        private void UpdateHPBar()
        {
            HPBar.SetValue(_heroHealth.Current, _heroHealth.Max);
        }
    }
}