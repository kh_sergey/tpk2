﻿using System;
using System.Linq;
using System.Xml;
using CodeBase.Logic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Editor
{
    [CustomEditor(typeof(UnityUniqueId))]
    public class UniqueIdEditor : UnityEditor.Editor
    {
        private void OnEnable()
        {
            var uniqueId = (UnityUniqueId) target;

            if (IsPrefab(uniqueId))
                return;
            
            if (string.IsNullOrEmpty(uniqueId.Id))
                Generate(uniqueId);
            else
            {
                UnityUniqueId[] uniqueIds = FindObjectsOfType<UnityUniqueId>();

                if (uniqueIds.Any(other => other != uniqueId && other.Id == uniqueId.Id))
                    Generate(uniqueId);
            }
        }

        private bool IsPrefab(UnityUniqueId uniqueId) => 
            uniqueId.gameObject.scene.rootCount == 0; // Если на сцене инспектированного объекта нет никаких корневых объектов - это префаб

        private void Generate(UnityUniqueId uniqueId)
        {
            uniqueId.Id = $"{uniqueId.gameObject.scene.name}_{Guid.NewGuid().ToString()}";

            if (!Application.isPlaying)
            {
                EditorUtility.SetDirty(uniqueId);
                EditorSceneManager.MarkSceneDirty(uniqueId.gameObject.scene);
            }
        }
    }
}