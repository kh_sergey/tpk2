﻿using System;
using UnityEngine;

namespace DefaultNamespace
{
    public class GlobalVariables : MonoBehaviour
    {
        public static GlobalVariables Instance;

        public GameObject joystick;
        
        private void Start()
        {
            Instance = this;
        }
    }
}